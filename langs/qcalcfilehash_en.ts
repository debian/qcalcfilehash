<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.ui" line="182"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="33"/>
        <source>Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="46"/>
        <source>Algorithm:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="109"/>
        <source>Calc</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="26"/>
        <source>Path:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="129"/>
        <source>Check:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="149"/>
        <source>Copyright 2017-2022 DanSoft. All rights reserved.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="172"/>
        <source>Compare the hash</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="42"/>
        <source>GOST R 34.11-94</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="43"/>
        <source>GOST R 34.11-2012 (256 bit)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="44"/>
        <source>GOST R 34.11-2012 (512 bit)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="82"/>
        <source>Open file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="82"/>
        <source>All Files (*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="149"/>
        <source>equally</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="151"/>
        <source>different</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ObjectConsole</name>
    <message>
        <location filename="../ObjectConsole.cpp" line="26"/>
        <source>Invalid positional arguments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ObjectConsole.cpp" line="63"/>
        <source>Invalid hash argument</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ObjectConsole.cpp" line="67"/>
        <source>Filename: %s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ObjectConsole.cpp" line="97"/>
        <source>Result: %s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ObjectConsole.cpp" line="101"/>
        <location filename="../ObjectConsole.cpp" line="103"/>
        <source>Check: %s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ObjectConsole.cpp" line="101"/>
        <source>equally</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ObjectConsole.cpp" line="103"/>
        <source>different</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QCoreApplication</name>
    <message>
        <location filename="../main.cpp" line="83"/>
        <source>Source file to hash</source>
        <oldsource>Source file to hash.</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.cpp" line="84"/>
        <source>Hash algorithm</source>
        <oldsource>Hash algorithm.</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.cpp" line="87"/>
        <source>hash comparison</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.cpp" line="88"/>
        <source>hash</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.cpp" line="92"/>
        <source>Show progress</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.cpp" line="95"/>
        <source>Show list all hash algorithm</source>
        <oldsource>Show list all hash algorithm.</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.cpp" line="98"/>
        <source>Open in gui</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
