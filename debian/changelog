qcalcfilehash (1.1.1-1) unstable; urgency=medium

  * New upstream release.
  * debian/control: bumped Standards-Version to 4.6.2.
  * debian/copyright: updated some years.

 -- Fabio Augusto De Muzio Tobich <ftobich@debian.org>  Mon, 19 Dec 2022 09:39:21 -0300

qcalcfilehash (1.0.7+git20200816.1eb8770-3) unstable; urgency=medium

  * debian/control:
      - Bumped Standards-Version to 4.6.0.
      - Changed my email in Maintainer field.
  * debian/copyright:
      - Changed my email in packaging block.
      - Updated some years.
  * debian/salsa-ci.yml: use Debian recipe for salsa CI.
  * debian/upstream/metadata: removed unknown Homepage field.

 -- Fabio Augusto De Muzio Tobich <ftobich@debian.org>  Fri, 22 Oct 2021 13:28:21 -0300

qcalcfilehash (1.0.7+git20200816.1eb8770-2) unstable; urgency=medium

  * debian/control: bumped Standards-Version to 4.5.1.
  * debian/upstream/org.bitbucket.qcalcfilehash.metainfo.xml: screenshot url
    updated to fix AppStream issue warning, upstream changed url.

 -- Fabio Augusto De Muzio Tobich <ftobich@gmail.com>  Mon, 14 Dec 2020 10:58:45 -0300

qcalcfilehash (1.0.7+git20200816.1eb8770-1) unstable; urgency=medium

  * New upstream release.
  * Upload to unstable.
  * debian/control: added 'qt5-qmake:native' in Build-Depends field to prevent
    FTCBFS.
  * debian/copyright:
      - Removed 'create-man.sh' file block, it was removed from package.
      - Removed the BSD-3-Clause license block, not used anymore.
  * debian/manpage/: removed, manpage provided upstream now.
  * debian/manpages: updated manpage path.
  * debian/patches/: removed, the one patch was incorporated upstream.
  * debian/source/lintian-overrides: removed, not needed anymore.
  * debian/tests/control: changed last test to run a script instead a
    Test-Command and marked as superficial.
  * debian/tests/run: added to run a simple test.

 -- Fabio Augusto De Muzio Tobich <ftobich@gmail.com>  Sun, 18 Oct 2020 00:32:05 -0300

qcalcfilehash (1.0.6+git20200806.0d09090-1) experimental; urgency=medium

  * Initial release. (Closes: #944278)

 -- Fabio Augusto De Muzio Tobich <ftobich@gmail.com>  Fri, 07 Aug 2020 17:52:35 -0300
